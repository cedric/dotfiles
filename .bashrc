
# Append to .bashrc file

# Android studio
export PATH="$PATH:/opt/android-studio/bin/"

# Android SDK
export ANDROID_HOME=~/.android-sdk-linux # For Cordova
export PATH="$PATH:$ANDROID_HOME/platform-tools" # Add adb to the PATH

export PATH="$HOME/.node/bin:$PATH"
export NODE_PATH="$HOME/.node/lib/node_modules:$NODE_PATH"
export MANPATH="$HOME/.node/share/man:$MANPATH"

# Cabal 1.22.4
export CABAL_HOME=$HOME/.cabal
export PATH=$CABAL_HOME/bin:$PATH
