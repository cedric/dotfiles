(put 'upcase-region 'disabled nil)

;; Colors and fonts
(set-background-color "black")
(set-foreground-color "white")
(set-frame-font "9x15")

;; Disabling the Welcome Screen
(setq inhibit-startup-message t)

;; Turn wrapping on in horizontally-split windows
(setq truncate-partial-width-windows nil)

;; Starting emacs in full screen
(defun toggle-fullscreen ()
(interactive)
(x-send-client-message nil 0 nil "_NET_WM_STATE" 32
'(2 "_NET_WM_STATE_MAXIMIZED_VERT" 0))
(x-send-client-message nil 0 nil "_NET_WM_STATE" 32
'(2 "_NET_WM_STATE_MAXIMIZED_HORZ" 0))
)
(toggle-fullscreen)

;; Tell emacs where is your personal elisp lib dir
;; this is default dir for extra packages
(add-to-list 'load-path "~/.emacs.d/lisp")

;; load the packages, best not to include the ending ".el" or ".elc"
(load-file "~/.emacs.d/lisp/emacs-for-python/epy-init.el") ; from git://github.com/gabrielelanaro/emacs-for-python

(add-to-list 'load-path "~/.emacs.d/lisp/jdee-2.4.1/lisp")
(load "jde")
(put 'downcase-region 'disabled nil)
