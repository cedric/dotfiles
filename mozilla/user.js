user_pref("beacon.enabled", false);

user_pref("browser.ctrlTab.previews", true);
user_pref("browser.search.geoip.url", "");
user_pref("browser.search.showOneOffButtons", false);
user_pref("browser.search.suggest.enabled", false);
user_pref("browser.urlbar.trimURLs", false);
user_pref("browser.urlbar.formatting.enabled", false);

user_pref("datareporting.healthreport.uploadEnabled", false);

user_pref("extensions.pocket.enabled", false);

user_pref("geo.enabled", false);
user_pref("geo.wifi.uri", "");

// user_pref("javascript.enabled", false);

user_pref("loop.enabled", false);
user_pref("loop.throttled", false);

user_pref("media.eme.enabled", false);
user_pref("media.gmp-eme-adobe.enabled", false);
user_pref("media.peerconnection.enabled", false);
user_pref("middlemouse.contentLoadURL", false);

user_pref("network.dns.disablePrefetch", true);
user_pref("network.http.sendRefererHeader", 2);
user_pref("network.http.referer.spoofSource", true);
user_pref("network.http.referer.trimmingPolicy", 1);
user_pref("network.http.referer.XOriginPolicy", 1);
user_pref("network.http.speculative-parallel-limit", 0);
user_pref("network.IDN_show_punycode", true);
user_pref("network.prefetch-next", false);

user_pref("privacy.donottrackheader.enabled", true);
user_pref("privacy.firstparty.isolate", true);
user_pref("privacy.resistFingerprinting", true);
user_pref("privacy.trackingprotection.enabled", true);

user_pref("reader.parse-on-load.enabled", false);

user_pref("toolkit.telemetry.enabled", false);
user_pref("toolkit.telemetry.cachedClientID", "");
