#!/bin/sh

### Upgrade packages that are available to update
apt-get update
apt-get -y upgrade


### Binary firmware for various drivers in the Linux kernel 
apt-get install -y firmware-linux
apt-get install -y intel-microcode


### Fonts
apt-get install -y ttf-freefont ttf-bitstream-vera ttf-dejavu ttf-liberation


### Editors
apt-get install -y emacs
apt-get install -y vim


### LaTeX
apt-get install -y latex texlive-latex-recommended texlive-latex-extra texlive-lang-french texlive-fonts-recommended
apt-get install -y kile

### PDF
apt-get install -y flpsed

### MySpell languages
apt-get install -y myspell-en-gb myspell-en-us myspell-fr-gut


### Security and privacy
apt-get install -y clamav
apt-get install -y pass
apt-get install -y tor privoxy
apt-get install -y kleopatra kgpg


### Graphics
apt-get install -y gimp inkscape dia krita exiftool potrace


### Audio
apt-get install -y amarok


### Analysis
apt-get install -y tcpdump

### Development
apt-get install -y build-essential
apt-get install -y libssl-dev openssl
apt-get install -y libsqlite3-dev
apt-get install -y sqlitebrowser
apt-get install -y git mercurial
apt-get install -y umbrello
apt-get install -y poedit
apt-get install -y cloc


### Games
#add-apt-repository -y deb http://httpredir.debian.org/debian/ jessie main contrib non-free
#dpkg --add-architecture i386
#apt-get update
#apt-get install -y steam


### MISCELANOUS
apt-get install -y vlc recordmydesktop
