#!/bin/sh
# firewall.sh - Configurable per-host firewall for workstations and
# servers.(c) 2003 Tero Karvinen - tero karvinen at iki fi - GPL
# Cleanup old rules # All the time firewall is in a secure, closed state
iptables -P INPUT DROP
iptables -P FORWARD DROP
iptables --flush        # Flush all rules, but keep policies
iptables --delete-chain
## Workstation Minimal firewall
iptables -P FORWARD DROP
iptables -P INPUT DROP
iptables -A INPUT -i lo --source 127.0.0.1 --destination 127.0.0.1 -j ACCEPT
iptables -A INPUT -m state --state "ESTABLISHED,RELATED" -j ACCEPT
iptables -A INPUT -p icmp --icmp-type destination-unreachable -j ACCEPT
iptables -A INPUT -p icmp --icmp-type time-exceeded -j ACCEPT
iptables -A INPUT -p icmp --icmp-type echo-request -j ACCEPT
iptables -A INPUT -p icmp --icmp-type echo-reply -j ACCEPT


## Maximum 10 HTTP connection
iptables -A INPUT -p tcp --dport 80 -m connlimit --connlimit-above 10 --connlimit-mask 28 -j DROP


## No more than 3 TCP syn per second.
iptables -A INPUT -p tcp --dport 80 --tcp-flags SYN SYN -m limit \
            --limit 3/second --limit-burst 7 -j ACCEPT
iptables  -A INPUT -p tcp --dport 80 --tcp-flags SYN SYN -j DROP


####### HOLES #######
# For Samba
iptables -A INPUT -p tcp --dport 445 -j ACCEPT # Microsoft-DS, if you are using Active Directory
iptables -A INPUT -p tcp --dport 139 -j ACCEPT # NETBIOS session service
iptables -A INPUT -p udp --dport 137 -j ACCEPT # NETBIOS Name Service
iptables -A INPUT -p udp --dport 138 -j ACCEPT # NETBIOS Datagram Service


iptables -A INPUT -p tcp --dport 5000 -j ACCEPT


# For foban
iptables -A INPUT -p udp --dport 12555 --destination 255.255.255.255 -j ACCEPT
iptables -A INPUT -p tcp --dport 12555 -j ACCEPT

# For pyAggr3g470r
#iptables -A INPUT -p tcp --dport 12556 -j ACCEPT

# For PyCarpooling
#iptables -A INPUT -p tcp --dport 12558 -j ACCEPT

# For Security Assurance Evaluation System
#iptables -A INPUT -p tcp --dport 12557 -j ACCEPT

# For the WiSafeCar portal
#iptables -A INPUT -p tcp --dport 8000 -j ACCEPT


#iptables -A INPUT -p tcp --dport ssh -j ACCEPT
#iptables -A INPUT -p tcp --dport http -j ACCEPT
#iptables -A INPUT -p tcp --dport https -j ACCEPT
#####################


#iptables -t nat -A OUTPUT -p tcp --dport 80 -m owner --uid-owner root -j ACCEPT
#iptables -t nat -A OUTPUT -p tcp --dport 80 -m owner ! --uid-owner privoxy -j REDIRECT --to-port 8118
#iptables -A INPUT -p tcp --dport 8118 -j ACCEPT


iptables -A INPUT -j LOG -m limit --limit 40/minute
iptables -A INPUT -j DROP


########### IP v6 #######################
ip6tables -P FORWARD DROP
ip6tables -P INPUT DROP




echo 1 > /proc/sys/net/ipv4/icmp_echo_ignore_all
