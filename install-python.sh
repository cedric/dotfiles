sudo apt-get install -y build-essential 
sudo apt-get install -y libssl-dev openssl # for pip
sudo apt-get install -y libsqlite3-dev  # for sqlite
sudo apt-get install -y tk-dev # for tkinter

PYTHON_VERSION=3.6.1

wget https://www.python.org/ftp/python/$PYTHON_VERSION/Python-$PYTHON_VERSION.tar.xz
tar -xf Python-$PYTHON_VERSION.tar.xz
rm Python-$PYTHON_VERSION.tar.xz
cd Python-$PYTHON_VERSION/
export PYTHONHOME=/usr/local/
export LD_RUN_PATH=/usr/local/lib/
./configure --enable-loadable-sqlite-extensions --enable-shared --enable-optimizations
make
sudo make install
cd ..
sudo rm -Rf Python-$PYTHON_VERSION/
